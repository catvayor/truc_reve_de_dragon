/////////////HTML elements
//in
var fiche_sel = document.getElementById('fiche_sel');
var carac_sel = document.getElementById('carac_sel');
var comp_sel = document.getElementById('comp_sel');

var base_level_in = document.getElementById('base');
var comp_in = document.getElementById('comp');
var diff_in = document.getElementById('difficulty');

//out
var part_out = document.getElementById('part');
var signi_out = document.getElementById('signi');
var reu_out = document.getElementById('reussite');
var echec_out = document.getElementById('echec_tot');

/////////////Global vars
var table = [];
var fiche = {};

/////////////functions
function make_option(val,print){ return "<option value=\"" + val + "\">" + print + "</option>"; }

function update_fiche(){
    $.getJSON("./"+fiche_sel.value, function(obj) {
        var carac_old = carac_sel.value;
        var comp_old = comp_sel.value;
        fiche = {};
        carac_sel.innerHTML = "";
        comp_sel.innerHTML = "";

        for(var carac in obj.carac){
            fiche["carac/" + carac] = obj.carac[carac];
            carac_sel.innerHTML += make_option("carac/" + carac, carac + " (" + obj.carac[carac] + ")");
        }
        for(var comp_cat in obj.comp){
            var comp_obj = obj.comp[comp_cat];
            if(typeof comp_obj === "number"){
                fiche["comp/" + comp_cat] = comp_obj;
                comp_sel.innerHTML += make_option("comp/" + comp_cat, comp_cat + " (" + comp_obj + ")");
                continue;
            }
            comp_sel.innerHTML +=  "<optgroup label=\"" + comp_cat + "\">";
            for(var comp in comp_obj){
                fiche["comp/" + comp_cat + "/" + comp] = comp_obj[comp];
                comp_sel.innerHTML += make_option("comp/" + comp_cat + "/" + comp, comp + " (" + comp_obj[comp] + ")");
            }
            comp_sel.innerHTML +=  "</optgroup>";
        }

        if(carac_old in fiche)
            carac_sel.value = carac_old;
        if(comp_old in fiche)
            comp_sel.value = comp_old;

        set_comp();
        set_carac();
    });
}

function set_carac(){
    base_level_in.value = fiche[carac_sel.value];
    resolution();
}

function set_comp(){
    comp_in.value = fiche[comp_sel.value];
    resolution();
}

function resolution(){
    var base_level = base_level_in.value - 0;
    var comp = comp_in.value - 0;
    var difficulty = diff_in.value - 0;
    
    var total_difficulty = Math.max(-10, Math.min(difficulty + comp, 7));
    var entry = table[base_level - 1][total_difficulty + 10];

    part_out.innerHTML = entry.part;
    signi_out.innerHTML = Math.floor(entry.reussite/2);
    reu_out.innerHTML = entry.reussite;
    echec_out.innerHTML =  entry.echec_tot < 0 ? "Pas d'échecs possible" : entry.echec_tot;
}

/////////////init
var fiches = {
    "derek.json":"Derek Harkonnen",
    "priscilla.json":"Priscilla Daven",
    "fedor.json":"Fédor Khorassian",
    "joella.json":"Joella Rasha",
    "leo.json":"Leo Marth",
    "eddar.json":"Eddar Renogari"
};

$.getJSON("./table.json", function(obj) {
    table = obj;
    resolution();
    for(var key in fiches){
        fiche_sel.innerHTML += make_option(key, fiches[key]);
    }
    update_fiche();
});
